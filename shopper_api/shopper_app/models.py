from django.db import models


# Create your models here.

class Rating(models.Model):
    value = models.DecimalField(max_digits=3, decimal_places=2)
    count = models.IntegerField()

    class Meta:
        db_table = "app_user_rating"


class User(models.Model):
    name = models.CharField(max_length=100)
    username = models.CharField(max_length=20)
    document = models.CharField(max_length=11)
    phone = models.CharField(max_length=11)
    email = models.CharField(max_length=50)
    password = models.CharField(max_length=10)
    street = models.CharField(max_length=100)
    city = models.CharField(max_length=20)
    state = models.CharField(max_length=20)
    credit = models.IntegerField()
    bid_count = models.IntegerField()
    wish_count = models.IntegerField()
    rating = models.ForeignKey(Rating, on_delete=models.CASCADE)

    class Meta:
        db_table = "app_user"

    def __str__(self):
        return str({
            "User": {
                "name": str(self.name),
                "document": str(self.document),
                "phone": str(self.phone),
                "email": str(self.email),
                "username": str(self.username),
                "password": str(self.password),
                "street": str(self.street),
                "city": str(self.city),
                "state": str(self.state),
                "credit": str(self.credit),
                "bid_count": str(self.bid_count),
                "wish_count": str(self.wish_count)
            }
        })

    def __repr__(self):
        return str({
            "User": {
                "name": str(self.name),
                "document": str(self.document),
                "phone": str(self.phone),
                "email": str(self.email),
                "username": str(self.username),
                "password": str(self.password),
                "street": str(self.street),
                "city": str(self.city),
                "state": str(self.state),
                "credit": str(self.credit),
                "bid_count": str(self.bid_count),
                "wish_count": str(self.wish_count)
            }
        })


class Wish(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=250)
    image_url = models.CharField(max_length=50)
    status = models.CharField(max_length=10)
    category = models.CharField(max_length=10)
    bid_count = models.IntegerField()
    deadline = models.DateTimeField()

    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    class Meta:
        db_table = "app_wish"

    def __str__(self):
        return str({
            "Wish": {
                "name": str(self.name),
                "description": str(self.description),
                "image_url": str(self.image_url),
                "category": str(self.category),
                "status": str(self.status),
                "user": str(self.user),
                "bid_count": str(self.bid_count),
                "deadline": str(self.deadline),
            }
        })

    def __repr__(self):
        return str({
            "Wish": {
                "name": str(self.name),
                "description": str(self.description),
                "image_url": str(self.image_url),
                "category": str(self.category),
                "status": str(self.status),
                "user": str(self.user),
                "bid_count": str(self.bid_count),
                "deadline": str(self.deadline),
            }
        })


class Bid(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    wish = models.ForeignKey(Wish, on_delete=models.CASCADE)
    value = models.DecimalField(max_digits=9, decimal_places=2)
    status = models.CharField(max_length=10)

    class Meta:
        db_table = "app_bid"

    def __str__(self):
        return str({
            "Bid": {
                "user": str(self.user),
                "wish": str(self.wish),
                "value": str(self.value),
                "status": str(self.status),
            }
        })

    def __repr__(self):
        return str({
            "Bid": {
                "user": str(self.user),
                "wish": str(self.wish),
                "value": str(self.value),
                "status": str(self.status),
            }
        })
