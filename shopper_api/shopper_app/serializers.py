from rest_framework import serializers
from .models import User, Wish


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id",
                  "name",
                  "document",
                  "username",
                  "password",
                  "email")

    def to_representation(self, instance):
        return {
            "User": {
                "id": str(instance.id),
                "name": str(instance.name),
                "document": str(instance.document),
                "phone": str (instance.phone),
                "email": str(instance.email),
                "street": str(instance.street),
                "city": str(instance.city),
                "state": str(instance.state),
                "username": str(instance.username),
                "credit": str(instance.credit),
                "wish_count": str(instance.wish_count),
                "bid_count": str(instance.bid_count),
                "Rating": {
                    "value": str(instance.rating.value),
                },
            }
        }


class WishSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wish
        fields = ("name",
                  "description",
                  "image_url",
                  "category",
                  "status")

    def to_representation(self, instance):
        return {
            "Wish": {
                "id": str(instance.id),
                "name": str(instance.name),
                "description": str(instance.description),
                "image_url": str(instance.image_url),
                "category": str(instance.category),
                "status": str(instance.status),
                "bid_count": str(instance.bid_count),
                "deadline": str(instance.deadline),
                "User": {
                    "id": str(instance.user.id),
                    "username": str(instance.user.username),
                    "name": str(instance.user.name),
                    "phone": str(instance.user.phone),
                    "street": str(instance.user.street),
                    "city": str(instance.user.city),
                    "state": str(instance.user.state),
                    "Rating": {
                        "value": str(instance.user.rating.value),
                    },
                },
            }
        }


class BidSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wish
        fields = ("value",
                  "status")

    def to_representation(self, instance):
        return {
            "Bid": {
                "id": str(instance.id),
                "value": str(instance.value),
                "status": str(instance.status),
                "User": {
                    "id": str(instance.user.id),
                    "name": str(instance.user.name),
                    "Rating": {
                        "value": str(instance.user.rating.value),
                    },
                },
                "Wish": {
                    "id": str(instance.wish.id),
                    "name": str(instance.wish.name),
                    "description": str(instance.wish.description),
                    "image_url": str(instance.wish.image_url),
                    "category": str(instance.wish.category),
                    "status": str(instance.wish.status),
                    "User": {
                        "id": str(instance.wish.user.id),
                        "name": str(instance.wish.user.name),
                        "phone": str(instance.user.phone),
                        "street": str(instance.wish.user.street),
                        "city": str(instance.wish.user.city),
                        "state": str(instance.wish.user.state),
                        "Rating": {
                            "value": str(instance.wish.user.rating.value),
                        },
                    }
                }
            }
        }
