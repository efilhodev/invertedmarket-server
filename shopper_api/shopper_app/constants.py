class WishStatus(object):
    AVAILABLE = "AVAILABLE"
    FINISHED = "FINISHED"


class WishCategory(object):
    ALL = "ALL"
    ELECTRONICS = "ELECTRONICS"
    CLOTHES = "CLOTHES"
    ACCESSORY = "ACCESSORY"


class BidStatus(object):
    SENT = "SENT"
    ACCEPTED = "ACCEPTED"
    CLOSED = "CLOSED"
    CANCELLED = "CANCELLED"
