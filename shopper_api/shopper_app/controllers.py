from django.db import transaction
from .models import User, Wish, Bid, Rating
from .constants import WishStatus, BidStatus
from datetime import datetime, timedelta
import decimal


class UserController:
    @staticmethod
    def business_user_create(name, document, phone, email, username, password, street, city, state):
        with transaction.atomic():
            user = User()
            user.name = name
            user.document = document
            user.phone = phone
            user.email = email
            user.username = username
            user.password = password
            user.street = street
            user.city = city
            user.state = state
            user.credit = 50
            user.bid_count = 0
            user.wish_count = 0

            rating = Rating()
            rating.count = 0
            rating.value = 0.00
            rating.save()

            user.rating = rating
            user.save()

        return user

    @staticmethod
    def business_user_login(username, password):
        user = User.objects.get(username=username, password=password)

        return user

    @staticmethod
    def business_rating_user(received_value, user_id):
        with transaction.atomic():
            user = User.objects.get(id=user_id)
            rating = user.rating

            rating.count = rating.count + 1
            rating.value = (decimal.Decimal(rating.value) + decimal.Decimal(received_value)) / decimal.Decimal(
                rating.count)
            rating.save()

            user.rating = rating
            user.save()

        return user

    @staticmethod
    def business_user_by_id(user_id):
        return User.objects.get(id=user_id)


class WishController:
    @staticmethod
    def business_wish_create(name, description, image_url, category, user_id):
        with transaction.atomic():
            wish = Wish()
            wish.name = name
            wish.description = description
            wish.image_url = image_url
            wish.category = category
            wish.user = UserController.business_user_by_id(user_id)
            wish.bid_count = 0
            wish.status = WishStatus.AVAILABLE
            wish.deadline = datetime.now() + timedelta(days=1)
            wish.save()

            user = UserController.business_user_by_id(user_id)
            user.wish_count = user.wish_count + 1
            user.save()

        return wish

    @staticmethod
    def business_wish_list_all():
        wishes = Wish.objects.all().exclude(status=WishStatus.FINISHED)

        return wishes

    @staticmethod
    def business_wish_list_by_category(category):
        try:
            wishes = Wish.objects.filter(category=category).exclude(status=WishStatus.FINISHED)
        except:
            pass

        return wishes

    @staticmethod
    def business_wish_list_by_user_id(user_id):
        wishes = Wish.objects.filter(user=user_id)

        return wishes

    @staticmethod
    def business_wish_by_id(wish_id):
        wish = Wish.objects.get(id=wish_id)

        return wish

    @staticmethod
    def business_wish_delete(wish_id):
        with transaction.atomic():
            Wish.objects.get(id=wish_id).delete()


class BidController:
    @staticmethod
    def business_bid_create(user_id, wish_id, value):
        with transaction.atomic():

            user = UserController.business_user_by_id(user_id)
            if user.credit < 1:
                return None
            try:
                old_bid = Bid.objects.get(user=user_id, wish_id=wish_id, status=BidStatus.SENT)
                if old_bid is not None:
                    BidController.business_bid_cancel(old_bid.id)
            except:
                pass

            finally:
                new_bid = Bid()
                new_bid.user = UserController.business_user_by_id(user_id)
                new_bid.wish = WishController.business_wish_by_id(wish_id)
                new_bid.value = value
                new_bid.status = BidStatus.SENT
                new_bid.save()

                wish = WishController.business_wish_by_id(wish_id)
                wish.bid_count = wish.bid_count + 1
                wish.save()

                user.credit = user.credit - 1
                user.bid_count = user.bid_count + 1
                user.save()

        return new_bid

    @staticmethod
    def business_bid_list_by_wish(wish_id):
        bids = Bid.objects.filter(wish=wish_id, status=BidStatus.SENT)

        return bids

    @staticmethod
    def business_bid_list_by_user(user_id):
        bids = Bid.objects.filter(user=user_id)

        return bids

    @staticmethod
    def business_bid_accept(bid_id):
        with transaction.atomic():
            bid = Bid.objects.get(id=bid_id)
            bid.status = BidStatus.ACCEPTED
            bid.wish.status = WishStatus.FINISHED

            bids = Bid.objects.filter(wish_id=bid.wish.id).exclude(id=bid_id)

            for other_bid in bids:
                other_bid.status = BidStatus.CLOSED
                other_bid.save()

            bid.wish.save()
            bid.save()

        return bid

    @staticmethod
    def business_bid_cancel(bid_id):
        with transaction.atomic():
            bid = Bid.objects.get(id=bid_id)
            bid.status = BidStatus.CANCELLED
            bid.save()

            wish = WishController.business_wish_by_id(bid.wish.id)
            wish.bid_count = wish.bid_count - 1
            wish.save()

        return bid

    @staticmethod
    def business_wish_bid_winner(wish_id):
        bid = Bid.objects.get(status=BidStatus.ACCEPTED, wish=wish_id)

        return bid
