from django.conf.urls import url
from .views import APIRegisterUserView, APILoginUserView, APIRegisterWishView, \
    APIListWishView, APIListWishByUserIdView, APIRegisterBidView, APIUserInfoView, \
    APIRatingUserView, APIListBidByWishView, APIBidAcceptedByUser, APIWishById, \
    APIListWishByCategoryView, APIListBidByUserView, APICancelBid, APIDeleteWish, APIGetBidWinner

urlpatterns = [

    url(
        regex=r'^user/register/$',
        view=APIRegisterUserView.as_view(),
        name='user_register'),

    url(
        regex=r'^user/info/$',
        view=APIUserInfoView.as_view(),
        name='user_info'),

    url(
        regex=r'^user/rating/$',
        view=APIRatingUserView.as_view(),
        name='user_rating'),

    url(
        regex=r'^user/login/$',
        view=APILoginUserView.as_view(),
        name='user_login'),

    url(
        regex=r'^wish/register/$',
        view=APIRegisterWishView.as_view(),
        name='wish_register'),

    url(
        regex=r'^wish/all/$',
        view=APIListWishView.as_view(),
        name='wishes_all'),

    url(
        regex=r'^wish/category/$',
        view=APIListWishByCategoryView.as_view(),
        name='wishes_by_category'),

    url(
        regex=r'^wish/by_user_id/$',
        view=APIListWishByUserIdView.as_view(),
        name='wishes_by_user_id'),

    url(
        regex=r'^wish/by_id/$',
        view=APIWishById.as_view(),
        name='wish_by_id'),

    url(
        regex=r'^wish/delete/$',
        view=APIDeleteWish.as_view(),
        name='wish_delete'),

    url(
        regex=r'^bid/register/$',
        view=APIRegisterBidView.as_view(),
        name='wishes_by_user_id'),

    url(
        regex=r'^bid/by_wish_id/$',
        view=APIListBidByWishView.as_view(),
        name='bids_by_wish_id'),

    url(
        regex=r'^bid/by_user_id/$',
        view=APIListBidByUserView.as_view(),
        name='bids_by_user_id'),

    url(
        regex=r'^bid/accept/$',
        view=APIBidAcceptedByUser.as_view(),
        name='bids_accept'),

    url(
        regex=r'^bid/cancel/$',
        view=APICancelBid.as_view(),
        name='bids_cancel'),

    url(
        regex=r'^bid/winner/$',
        view=APIGetBidWinner.as_view(),
        name='bids_winner'),
]
