from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import HttpResponse
from .controllers import UserController, WishController, BidController
from .serializers import UserSerializer, WishSerializer, BidSerializer


# Create your views here.
class APIRegisterUserView(APIView):

    @staticmethod
    def post(request):
        user_saved = UserController.business_user_create(request.POST["name"],
                                                         request.POST["document"],
                                                         request.POST["phone"],
                                                         request.POST["email"],
                                                         request.POST["username"],
                                                         request.POST["password"],
                                                         request.POST["street"],
                                                         request.POST["city"],
                                                         request.POST["state"])

        user_serialized = UserSerializer(user_saved).data

        return Response(user_serialized)


class APILoginUserView(APIView):

    @staticmethod
    def post(request):
        username = request.POST["username"]
        password = request.POST["password"]

        user = UserController.business_user_login(username, password)

        user_serialized = UserSerializer(user).data

        return Response(user_serialized)


class APIUserInfoView(APIView):

    @staticmethod
    def get(request):
        user_id = request.GET["user_id"]

        user = UserController.business_user_by_id(user_id)

        user_serialized = UserSerializer(user).data

        return Response(user_serialized)


class APIRatingUserView(APIView):
    @staticmethod
    def post(request):
        user_id = request.POST["user_id"]
        received_value = request.POST["rating_value"]

        user = UserController.business_rating_user(received_value, user_id)

        user_serialized = UserSerializer(user).data

        return Response(user_serialized)


class APIRegisterWishView(APIView):

    @staticmethod
    def post(request):
        wish_saved = WishController.business_wish_create(request.POST["name"],
                                                         request.POST["description"],
                                                         request.POST["image_url"],
                                                         request.POST["category"],
                                                         request.POST["user_id"])

        wish_serialized = WishSerializer(wish_saved).data

        return Response(wish_serialized)


class APIWishById(APIView):

    @staticmethod
    def get(request):
        wish_id = request.GET["wish_id"]

        wish = WishController.business_wish_by_id(wish_id)

        wish_serialized = WishSerializer(wish).data

        return Response(wish_serialized)


class APIListWishView(APIView):

    @staticmethod
    def get(request):
        wishes = list(WishController.business_wish_list_all())

        wishes_serialized = WishSerializer(wishes, many=True).data

        return Response(wishes_serialized)


class APIListWishByUserIdView(APIView):

    @staticmethod
    def get(request):
        user_id = request.GET['user_id']
        wishes = list(WishController.business_wish_list_by_user_id(user_id))

        wishes_serialized = WishSerializer(wishes, many=True).data

        return Response(wishes_serialized)


class APIListWishByCategoryView(APIView):

    @staticmethod
    def get(request):
        category = request.GET['category']
        wishes = list(WishController.business_wish_list_by_category(category))

        wishes_serialized = WishSerializer(wishes, many=True).data

        return Response(wishes_serialized)


class APIDeleteWish(APIView):

    @staticmethod
    def post(request):
        try:
            wish_id = request.POST["wish_id"]
            WishController.business_wish_delete(wish_id)
        except:
            return HttpResponse(status=500)

        return HttpResponse(status=200)


class APIRegisterBidView(APIView):

    @staticmethod
    def post(request):
        bid_saved = BidController.business_bid_create(request.POST["user_id"],
                                                      request.POST["wish_id"],
                                                      request.POST["value"])

        if bid_saved is None:
            return HttpResponse(status=500)

        bid_serialized = BidSerializer(bid_saved).data

        return Response(bid_serialized)


class APIListBidByWishView(APIView):

    @staticmethod
    def get(request):
        wish_id = request.GET['wish_id']
        bids = list(BidController.business_bid_list_by_wish(wish_id))

        bids_serialized = BidSerializer(bids, many=True).data

        return Response(bids_serialized)


class APIListBidByUserView(APIView):

    @staticmethod
    def get(request):
        user_id = request.GET['user_id']
        bids = list(BidController.business_bid_list_by_user(user_id))

        bids_serialized = BidSerializer(bids, many=True).data

        return Response(bids_serialized)


class APIBidAcceptedByUser(APIView):

    @staticmethod
    def post(request):
        bid_id = request.POST['bid_id']

        bid_updated = BidController.business_bid_accept(bid_id)

        bid_updated_serialized = BidSerializer(bid_updated).data

        return Response(bid_updated_serialized)


class APICancelBid(APIView):

    @staticmethod
    def post(request):
        bid_id = request.POST['bid_id']
        bid_updated = BidController.business_bid_cancel(bid_id)

        bid_updated_serialized = BidSerializer(bid_updated).data

        return Response(bid_updated_serialized)


class APIGetBidWinner(APIView):

    @staticmethod
    def get(request):
        wish_id = request.GET['wish_id']

        bid_winner = BidController.business_wish_bid_winner(wish_id)

        bid_winner_serialized = BidSerializer(bid_winner).data

        return Response(bid_winner_serialized)
